package ThreadPrinter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;

public class ThreadPrinter {
    public static void main(String[] args) throws Exception {
        for(int x = 0; x < 5; x ++){
            String name = String.valueOf(x);
            Printer p = new Printer(name);
            p.start();
        }
    }
}

class Printer implements Runnable {
    private PrinterJob pj = PrinterJob.getPrinterJob();

    //the default page dimensions are 8 inches x 11 inches with 1 inch margins
    private PageFormat pf = pj.defaultPage();
    private Paper paper = new Paper();

    private Thread t;
    private String threadName;

    Printer(String name) {
        threadName = name + ".png";
    }

    public void run() {
        try {
            //for width and height, 1 = 1/72 inch
            paper.setImageableArea(0, 0, 216, 200);

            pf.setPaper(paper);

            pj.setPrintable(new MyPrintable2(threadName), pf);
            pj.print();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void start () {
        System.out.println("Starting " +  threadName );
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}

class MyPrintable2 implements Printable {
    private String filename;

    MyPrintable2(String filename) {
        this.filename = filename;
    }
    public int print(Graphics g, PageFormat pf, int pageIndex) {
        if (pageIndex != 0)
            return NO_SUCH_PAGE;
        BufferedImage img = null;
        Graphics2D g2 = (Graphics2D) g;
        try {
            img = ImageIO.read(new File(filename));
        } catch (IOException e) {
            System.out.println(e);
        }
        g2.drawImage(img, null, 24, 0);
        return PAGE_EXISTS;
    }
}