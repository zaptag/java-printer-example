package HelloWorldPrinter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.print.*;
import java.io.File;
import java.io.IOException;

public class HelloWorldPrinter {
    public static void main(String[] args) throws Exception {
        PrinterJob pj = PrinterJob.getPrinterJob();

        //the default page dimensions are 8 inches x 11 inches with 1 inch margins
        PageFormat pf = pj.defaultPage();
        Paper paper = new Paper();

        //for width and height, 1 = 1/72 inch
        paper.setImageableArea(0, 0, 216, 200);

        pf.setPaper(paper);

        pj.setPrintable(new MyPrintable1(), pf);

        try {
            pj.print();
        } catch (PrinterException e) {
            System.out.println(e);
        }
    }
}

class MyPrintable1 implements Printable {
    public int print(Graphics g, PageFormat pf, int pageIndex) {
        if (pageIndex != 0)
            return NO_SUCH_PAGE;
        BufferedImage img = null;
        Graphics2D g2 = (Graphics2D) g;
        try {
            img = ImageIO.read(new File("sample.jpg"));
        } catch (IOException e) {
            System.out.println(e);
        }
        g2.drawImage(img, null, 24, 0);
        return PAGE_EXISTS;
    }
}
